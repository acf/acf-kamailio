<% local form, viewlibrary, page_info = ...
htmlviewfunctions = require("htmlviewfunctions")
html = require("acf.html")
%>

<% if form.value.result then
	local func = haserl.loadfile(page_info.viewfile:gsub("searchdatabase", "viewtable"))
	local table, column = string.match(form.value.id.value, "^([^.]*)%.(.*)")
	form.value.table = cfe({ value=table })
	form.value.fields = cfe({ value={} })
	for i,o in ipairs(form.value.id.option) do
		local t,c = string.match(o, "^([^.]*)%.(.*)")
		if t == table then
			form.value.fields.value[#form.value.fields.value + 1] = c
		end
	end
	form.value.entries = form.value.result
	func(form, viewlibrary, page_info, session)
	form.value.entries = nil
	form.value.table = nil
	form.value.fields = nil
end %>

<%
	form.value.result = nil
	form.option = "Search"
	htmlviewfunctions.displayitem(form, page_info)
%>
