local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.listfiles(self)
	return self.model.list_files()
end

function mymodule.edit(self)
	return self.handle_form(self, self.model.get_filedetails, self.model.update_filedetails, self.clientdata, "Save", "Edit File", "File Saved")
end

function mymodule.listusers(self)
	return self.model.list_users()
end

function mymodule.createuser(self)
	return self.handle_form(self, self.model.get_new_user, self.model.create_new_user, self.clientdata, "Create", "Create New User")
end

function mymodule.deleteuser(self)
	return self.handle_form(self, self.model.get_delete_user, self.model.delete_user, self.clientdata, "Delete", "Delete User")
	--, "User deleted")
end

function mymodule.updateuser(self)
	return self.handle_form(self, self.model.get_user, self.model.update_user, self.clientdata, "Update", "Update User")
end

function mymodule.createdatabase(self)
	return self.handle_form(self, self.model.get_create_database, self.model.create_database, self.clientdata, "Create", "Create Database")
end

function mymodule.searchdatabase(self)
	return self.handle_form(self, self.model.get_search_database, self.model.search_database, self.clientdata, "Search", "Search Database")
end

-- Use acf-db to allow editing of the database
dbcontrollerfunctions = require("dbcontrollerfunctions")
for n,f in pairs(dbcontrollerfunctions) do
	if n ~= "listdatabases" then
		mymodule[n] = f
	end
end

return mymodule
