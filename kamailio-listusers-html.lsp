<% local form, viewlibrary, page_info, session = ... %>
<% htmlviewfunctions = require("htmlviewfunctions") %>
<% html = require("acf.html") %>

<script type="text/javascript">
	if (typeof jQuery == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery-latest.js"><\/script>');
	}
</script>

<script type="text/javascript">
	if (typeof $.tablesorter == 'undefined') {
		document.write('<script type="text/javascript" src="<%= html.html_escape(page_info.wwwprefix) %>/js/jquery.tablesorter.js"><\/script>');
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		<% if viewlibrary.check_permission("deleteuser") or viewlibrary.check_permission("updateuser") then %>
		$("#list").tablesorter({headers: {0:{sorter: false}}, widgets: ['zebra']});
		<% else %>
		$("#list").tablesorter({widgets: ['zebra']});
		<% end %>
	});
</script>

<% htmlviewfunctions.displaycommandresults({"deleteuser", "updateuser"}, session) %>
<% htmlviewfunctions.displaycommandresults({"createuser"}, session, true) %>

<% local header_level = htmlviewfunctions.displaysectionstart(form, page_info) %>
<table id="list" class="tablesorter"><thead>
	<tr>
	<% if viewlibrary.check_permission("deleteuser") or viewlibrary.check_permission("updateuser") then %>
		<th>Action</th>
	<% end %>
		<th>User Name</th>
		<th>Domain</th>
		<th>Password</th>
	</tr>
</thead><tbody>
<% local username = cfe({ type="hidden", value="" }) %>
<% local domain = cfe({ type="hidden", value="" }) %>
<% local redir = cfe({ type="hidden", value=page_info.orig_action }) %>
<% for i,user in ipairs(form.value) do %>
	<tr>
	<% username.value = user.username %>
	<% domain.value = user.domain %>
	<% if viewlibrary.check_permission("deleteuser") or viewlibrary.check_permission("updateuser") then %>
		<td>
	<% if viewlibrary.check_permission("updateuser") then %>
		<% htmlviewfunctions.displayitem(cfe({type="link", value={username=username, domain=domain, redir=redir}, label="", option="Update", action="updateuser"}), page_info, -1) %>
	<% end %>
	<% if viewlibrary.check_permission("deleteuser") then %>
		<% htmlviewfunctions.displayitem(cfe({type="form", value={username=username, domain=domain}, label="", option="Delete", action="deleteuser"}), page_info, -1) %>
	<% end %>
		</td>
	<% end %>
		<td><%= html.html_escape(user.username) %></td>
		<td><%= html.html_escape(user.domain) %></td>
	<% if viewlibrary.check_permission("updateuser") then %>
		<td><%= html.html_escape(user.password) %></td>
	<% else %>
		<td>******</td>
	<% end %>
	</tr>
<% end %>
</tbody></table>
<% htmlviewfunctions.displaysectionend(header_level) %>

<% if viewlibrary and viewlibrary.dispatch_component and viewlibrary.check_permission("createuser") then
	viewlibrary.dispatch_component("createuser")
end %>
